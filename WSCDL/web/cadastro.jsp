<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">
        <title>CDL - Agenda</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="offcanvas.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            input[type="checkbox"][readonly] {
                pointer-events: none;
            }
        </style>  
    </head>

    <body class="bg-light">


        <main role="main" class="container" style="width: 50%">
            <div style="background: #3f454f; height: 80px">
                <img src="Icones/Logomarca.png" style="position: relative;top: 15px;left: 70px; width: 70px" width="50px" height="50px"/>
                <div style="position: relative; left: 75%; top: -15px">
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/WSCDL">DashBoard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cadastro</li>
                    <div class="col">
                        <div class="float-right">
                            <a href="/WSCDL/login"><i class="material-icons" style="color: black">exit_to_app</i></a>
                        </div>
                        <div class="float-right" style="margin-right: 7px">
                            <a href="/WSCDL/Start"><i class="material-icons" style="color: black">home</i></a>
                        </div>
                    </div>
                </ol>
            </nav>

            <form role="form" id="form1" action="/WSCDL/Create" method="POST" onsubmit="return submitForm(this);">
                <input name ="tipo" type="hidden" class="form-control underlined" value="Agendamento">
                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container">
                        <h6 class="border-bottom border-gray pb-2 mb-4">Dados B�sicos</h6>
                        <div class="form-row">
                            <label class="control-label">Tipo: </label>
                        </div>
                        <div class="form-row">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio1" name="radio1" value="Relacionamento" class="custom-control-input">
                                <label class="custom-control-label" for="radio1">Relacionamento</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio2" name="radio1" value="Prospeccao" class="custom-control-input">
                                <label class="custom-control-label" for="radio2">Prospec��o</label>
                            </div>
                        </div>
                        <div class="form-row ">
                            <div class="col">
                                <label class="control-label">Empresa</label>
                                <input name ="empresa" id="empresa" onblur="Url_change()" type="text" class="form-control underlined"  placeholder="Nome da Empresa que sera visitada" required> 
                            </div>
                            <div class="col">
                                <label class="control-label">Endere�o</label>
                                <input type="text" id="endereco" name="endereco" value="${endereco}" class="form-control underlined" required>    
                            </div>
                        </div>


                        <input type="hidden" name="funcionario" id="funcionario" value="0">
                        <input type="hidden" name="hora" id="hora" value="0">
                        <input type="hidden" name="data" id="hora" value="${data}">
                        <div class="form-row">
                            <!--                            <div class="col"> 
                                                            <label class="control-label">Data</label>
                                                            <input name ="data" type="date" class="form-control underlined"  placeholder="(DD/MM/YYYY)" required>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container">
                        <h6 class="border-bottom border-gray pb-2 mb-4">Contato</h6>
                        <div class="form-row">
                            <div class="col">
                                <label class="control-label">Nome</label>
                                <input type="text" id="Nome" value="${nome}" name="nome" class="form-control underlined"> 
                            </div>
                            <div class="col">
                                <label class="control-label">Fone Fixo</label>
                                <input type="text" id="fone" value="${fonefixo}" name="fone" class="form-control underlined"> 
                            </div>
                            <div class="col">
                                <label class="control-label">Celular</label>
                                <input type="text" id="celular" value="${celular}" name="celular" class="form-control underlined"> 
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label class="control-label">Email</label>
                                <input type="email" id="email" value="${email}" name="email" class="form-control underlined"> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="my-3 p-3 bg-white rounded box-shadow">
                    <h6 class="border-bottom border-gray pb-2 mb-0">Agenda ${time}</h6>
                    <div class="month">      
                        <ul>
                            <!--                            <li class="prev">&#10094;</li>
                                                        <li class="next">&#10095;</li>-->
                            <li id="mes">
                                August<br>
                                <span style="font-size:18px" id="ano">2017</span>
                            </li>
                        </ul>
                    </div>

                    <ul class="weekdays">
                        <li>Dom</li>
                        <li>Seg</li>
                        <li>Ter</li>
                        <li>Qua</li>
                        <li>Qui</li>
                        <li>Sex</li>
                        <li>Sab</li>

                    </ul>

                    <ul class="days" id="days">  
                        <a href="#"> <li> 1</li></a>
                    </ul>
                    ${table}
                </div>

                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container  text-center">
                        <button type="submit" type="button" class="btn btn-success center">Salvar</button>
                        <a href="/WSCDL/Start" ><button  type="button" class="btn btn-danger center">Cancelar</button></a>
                    </div>
                </div>

            </form>
        </main>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
        <script src="../../assets/js/vendor/popper.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <script src="offcanvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script>
            function submitForm() {
                return confirm('Deseja salvar este agendamento?');
            }

                                    function Url_change() {
                                        data = "${data}";
                                        Url_change(data);
                                    }
                                    function Url_change(data) {
                                        if (data == null) {
                                            data = "${data}";
                                        }
                                        document.location.href = "Create?data=" + data +
                                                "&empresa=" + document.getElementById("empresa").value +
                                                "&endereco=" + document.getElementById("endereco").value +
                                                "&Nome=" + document.getElementById("Nome").value +
                                                "&fone=" + document.getElementById("fone").value +
                                                "&email=" + document.getElementById("email").value +
                                                "&celular=" + document.getElementById("celular").value +
                                                "&relacionamento=" + document.getElementById("radio1").checked +
                                                "&prospeccao=" + document.getElementById("radio2").checked;
                                    }
                                    function sub() {
                                        document.getElementById("form1").submit();
                                    }



                                    $(function () {
                                        $("#fone").mask("(00) 0000-0000");
                                        $("#celular").mask("(00) 0 0000-0000");
                                        displayCalendar();
                                        var url_string = document.URL; //window.location.href
                                        var url = new URL(url_string);
                                        if (url.searchParams.get("empresa") != "") {
                                            document.getElementById("empresa").value = url.searchParams.get("empresa");
                                            if ("${empresa}" != "") {
                                                document.getElementById("empresa").value = "${empresa}";
                                            }
                                        }
                                        if (url.searchParams.get("Nome") != "") {
                                            document.getElementById("Nome").value = url.searchParams.get("Nome");
                                            if ("${nome}" != "") {
                                                document.getElementById("Nome").value = "${empresa}";
                                            }
                                        } else {
                                            document.getElementById("Nome").value = "${empresa}";
                                        }
                                        var xi = url.searchParams.get("relacionamento");
                                        if (xi == "true") {
                                            document.getElementById("radio1").checked = true;
                                        } else {
                                            document.getElementById("radio2").checked = true;
                                        }
                                        if ("${endereco}" == "") {
                                            document.getElementById("endereco").value = url.searchParams.get("endereco");
                                        }
                                        if ("${fonefixo}" == "") {
                                            document.getElementById("fone").value = url.searchParams.get("fone");
                                        }
                                        if ("${celular}" == "") {
                                            document.getElementById("celular").value = url.searchParams.get("celular");
                                        }
                                        if ("${email}" == "") {
                                            document.getElementById("email").value = url.searchParams.get("email");
                                        }
                                    });
                                    window.addEventListener('load', function () {

                                    }, false);
                                    var old_cell = 0;
                                    function fun(id, hora, cell) {
                                        var cell2 = old_cell;
                                        document.getElementById("funcionario").value = id;
                                        document.getElementById("hora").value = hora;
                                        document.getElementById(cell).style.backgroundColor = "green";
                                        old_cell = cell;
                                        document.getElementById(cell2).style.backgroundColor = "";
                                    }

                                    function displayCalendar() {


                                        var htmlContent = "";
                                        var FebNumberOfDays = "";
                                        var counter = 1;
                                        var dateNow = new Date();
                                        var month = dateNow.getMonth();
                                        var nextMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
                                        var prevMonth = month - 1;
                                        var day = dateNow.getDate();
                                        var year = dateNow.getFullYear();
                                        //Determing if February (28,or 29)  
                                        if (month == 1) {
                                            if ((year % 100 != 0) && (year % 4 == 0) || (year % 400 == 0)) {
                                                FebNumberOfDays = 29;
                                            } else {
                                                FebNumberOfDays = 28;
                                            }
                                        }

                                        // names of months and week days.
                                        var monthNames = ["Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                                        var dayNames = ["Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "Sabado"];
                                        var dayPerMonth = ["31", "" + FebNumberOfDays + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"]

                                        // days in previous month and next one , and day of week.
                                        var nextDate = new Date(nextMonth + ' 1 ,' + year);
                                        var weekdays = nextDate.getDay();
                                        var weekdays2 = weekdays;
                                        var numOfDays = dayPerMonth[month];
                                        // this leave a white space for days of pervious month.
                                        while (weekdays > 0) {
                                            htmlContent += "<li></li>";
                                            // used in next loop.
                                            weekdays--;
                                        }

                                        // loop to build the calander body.
                                        while (counter <= numOfDays) {

                                            // When to start new line.
                                            if (weekdays2 > 6) {
                                                weekdays2 = 0;
                                            }

                                            // if counter is current day.
                                            // highlight current day using the CSS defined in header.
                                            if (counter == ${day}) {
                                                htmlContent += "<a onclick=\"Url_change('')\"><li><span class=\"active\">" + counter + "</span></li></a>";
                                            } else {
                                                htmlContent += "<a onclick=\"Url_change('" + year + "-" + (month + 1) + "-" + counter +
                                                        "')\"><li>" + counter + "</li></a>";
                                            }

                                            weekdays2++;
                                            counter++;
                                        }

                                        document.getElementById("mes").innerHTML = monthNames[month] + "<br><span style=\"font-size:18px\" >" + year + "</span>";
                                        document.getElementById("days").innerHTML = htmlContent;
                                    }
        </script>
    </body>
</html>
