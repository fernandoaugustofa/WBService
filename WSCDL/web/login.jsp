<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>
			Login
		</title>
		<meta name="description" content="">

        <link href="offcanvas.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</head>
	<body class="bg-light">
    <main role="main" class="container text-center" style="width: 30%">
        <img class="mr-3" src="banner.png"  alt="" width="100%" height="80px">

     <div class="my-3 p-3 bg-white rounded box-shadow">
       <h5 class="text-center ">LOGIN</h5>
        <div class="container" style="width: 40%">
         <form id="login-form"  action="login" method="post" novalidate="">
        <div class="form-group">
          <label for="user" >Usuario</label>
          <input type="text" class="form-control underlined" name="user" id="username" placeholder="Seu usuario" required>
        </div>
        <div class="form-group">
          <label for="password" >Senha</label>
          <input type="password" class="form-control underlined" name="password" id="password" placeholder="Senha" required>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-primary">Login</button>
        </div>
      </form>
        </div>
      </div>
  </main>
    <script src="js/vendor.js"></script>
    <script src="js/app.js"></script>
	</body>
</html>