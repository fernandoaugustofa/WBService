<!--Template Name: simple-plain-login
File Name: index.html
Author Name: ThemeVault
Author URI: http://www.themevault.net/
Licence URI: http://www.themevault.net/license/
-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
        <!-- Bootstrap core CSS -->
        <link rel="icon" href="images/favicon.png"/>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">    
        <!-- Custom styles for this template -->
        <link href="style.css" rel="stylesheet">
    </head>
    <body class="content" style="background: #eaeaea">
        <header>
            <div class="logo text-center">
                <img src="Icones/Logomarca.png" width="" height="100px"/>
                <h2>${user}</h2>
            </div>
        </header>
        <section id="login-page" >
            <div class="row">
                <div class="login-page text-center">
                    <h3 class="tagline" style="background: #2a57b8">Login</h3>
                    <form d="login-form"  action="login" method="post">
                        <div class="row">
                            <div class="input-value">
                                <input class="form-control placeholder-fix" type="text" placeholder="Usuario" name="user" required>
                                <input class="form-control placeholder-fix" type="password" placeholder="Senha" name="password" required>
                            </div>
                            <div class="action-button" >
                                <button class="btn-block" style="background: #2a57b8" type="submit">Login</button>        
                            </div>
                        </div>
                        
                    </form>
                </div>
                
                 <div class="copyright-box">
                   
                </div>
            </div>
        </section>
    </body>
</html>