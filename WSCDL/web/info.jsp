<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>CDL - Agenda</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="offcanvas.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            input[type="checkbox"][readonly] {
                pointer-events: none;
            }
        </style>
    </head>

    <body class="bg-light">

<!--        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a class="navbar-brand" href="#">CDL - Agenda</a>
            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"> <span class="glyphicon glyphicon-home"></span> Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Novo Agendamento</a>
                    </li>
            </div>
        </nav>-->

        <main role="main" class="container">
            <img class="mr-3" src="banner.png"  alt="" width="100%" height="80px">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/WSCDL">DashBoard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informa��o Adicionais</li>
                    <div class="col">
                        <div class="float-right">
                            <a href="/WSCDL/login"><i class="material-icons" style="color: black">exit_to_app</i></a>
                        </div>
                        <div class="float-right" style="margin-right: 7px">
                            <a href="/WSCDL/Start"><i class="material-icons" style="color: black">home</i></a>
                        </div>
                    </div>
                </ol>

            </nav>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <div class="container">
                    <div class="row" style="margin: 2px">
                        <div class="col-sm">
                            <h5 class="name text-center">${Empresa} - ${h3Status}</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow" id="map" style="width:100%;height:400px;"></div>
            <script>
                function myMap() {
                    var myCenter = new google.maps.LatLng(${Stop_lat},${Stop_lon});
                    var mapCanvas = document.getElementById("map");
                    var mapOptions = {center: myCenter, zoom: 18};
                    var map = new google.maps.Map(mapCanvas, mapOptions);
                    var marker = new google.maps.Marker({
                        position: myCenter
                    });
                    marker.setMap(map);
                    var geocoder = new google.maps.Geocoder();
                    geocodeAddress(geocoder, map);
                }
                function geocodeAddress(geocoder, resultsMap) {

                    geocoder.geocode({'address': '${Endereco}'}, function (results, status) {
                        if (status === 'OK') {
                            resultsMap.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: resultsMap,
                                position: results[0].geometry.location
                            });
                        } else {
                            alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }

            </script>
            <div class="my-3 p-3 bg-white rounded box-shadow">
                <div class="container">
                    <div class="row" style="margin: 2+0px">
                        <div class="col-sm">
                            <h6 class="name text-center">${Endereco}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h5 class="border-bottom border-gray pb-2 mb-4">Informa��es Basicas:</h5> 
                <div class="container">
                    <div class="row" style="margin: 2+0px">
                        <div class="col-sm">
                            <h5 class="value text-center">Funcionario:</h5>
                            <h6 class="name text-center">${Funcionario}</h6>
                        </div>
                        <div class="col-sm">
                            <h5 class="value text-center" >Data: </h5>
                            <h6 class="name text-center"> ${Data} </h6>
                        </div>
                        <div class="col-sm">
                            <h5 class="value text-center" > Hora:</h5>
                            <h6 class="name text-center"> ${Hora}  </h6>
                        </div>
                        <div class="col-sm">
                            <h5 class="value text-center" >Horario Sa�da:</h5>
                            <h6 class="name text-center"> ${Hora_Inicio} </h6>
                        </div>
                        <div class="col-sm">
                            <h5 class="value text-center" >Horario Chegada:</h5>
                            <h6 class="name text-center"> ${Hora_Final} </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <h5 class="border-bottom border-gray pb-2 mb-4">Realizado:</h5> 
            <div class="container">
                <div class="row" style="margin: 20px">
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="1" onclick="return false" ${nfe} readonly>
                        <label class="custom-control-label" for="1">NF-E:</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="2" onclick="return false" ${cdl_celular} readonly>
                        <label class="custom-control-label" for="2">CDL Celular</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="3" onclick="return false" ${central_cobranca} readonly>
                        <label class="custom-control-label" for="3">Central De Cobran�a</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="4" onclick="return false" ${spc} readonly>
                        <label class="custom-control-label" for="4">SPC</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="5" onclick="return false" ${cdl_saude} readonly>
                        <label class="custom-control-label" for="5">CDL Sa�de</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="6" onclick="return false" ${escola_de_negocios} readonly>
                        <label class="custom-control-label" for="6">Escola De Negocios</label>
                    </div>
                    <div class="col-sm">
                        <input type="checkbox" class="custom-control-input" id="7" onclick="return false" ${certificado_digital} readonly>
                        <label class="custom-control-label" for="7">Certificado Digital</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <h5 class="border-bottom border-gray pb-2 mb-4">OBS.:</h5> 
            <div class="container">
                <div class="row" style="margin: 20px">
                    <h6>${obs}</h6>
                </div>
            </div>
        </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <script src="offcanvas.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYRNt0WmIXvB-1thegt4aWvGvLlDL5nao&callback=myMap"></script>
</body>
</html>
