<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">
        <title>CDL - Agenda</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="offcanvas.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            input[type="checkbox"][readonly] {
                pointer-events: none;
            }
        </style>  
    </head>

    <body class="bg-light">
        <main role="main" class="container">
            <div style="background: #3f454f; height: 80px">
                <img src="Icones/Logomarca.png" style="position: relative;top: 15px;left: 70px; width: 70px" width="50px" height="50px"/>
                <div style="position: relative; left: 75%; top: -15px">
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/WSCDL">DashBoard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cadastro</li>
                    <div class="col">
                        <div class="float-right">
                            <a href="/WSCDL/login"><i class="material-icons" style="color: black">exit_to_app</i></a>
                        </div>
                        <div class="float-right" style="margin-right: 7px">
                            <a href="/WSCDL/Start"><i class="material-icons" style="color: black">home</i></a>
                        </div>
                    </div>
                </ol>
            </nav>

            <form role="form" action="/WSCDL/Create" method="POST" onsubmit="return submitForm(this);">
                <input name ="tipo" type="hidden" class="form-control underlined" value="Visita">     
                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container">
                        <h6 class="border-bottom border-gray pb-2 mb-4">Dados B�sicos</h6>
                        <div class="row">
                            <div class="col">
                                <label class="control-label">Data</label>
                                <input name ="data_visita"  type="date" class="form-control underlined"  placeholder="DD/MM/YYYY" value="${data}" readonly required>
                            </div>
                            <div class="col">
                                <label class="control-label">Hora</label>
                                <input name ="hora_visita" id="time" type="time" class="form-control underlined"  placeholder="HH:MM" value="" readonly required>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="control-label">Tipo: </label>
                        </div>
                        <div class="form-row">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio1" name="radio1" class="custom-control-input" onclick="clicktipoRelacionamento()">
                                <label class="custom-control-label" for="radio1">Relacionamento</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio2" name="radio1" class="custom-control-input" onclick="clicktipopros()">
                                <label class="custom-control-label" for="radio2">Prospec��o</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="control-label">Origem: </label>
                        </div>
                        <div class="form-row">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio12" name="radio22" class="custom-control-input" >
                                <label class="custom-control-label" for="radio12">CDL</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="radio22" name="radio22" class="custom-control-input">
                                <label class="custom-control-label" for="radio22">Cliente</label>
                            </div>
                        </div>
                        <div class="form-row ">
                            <div class="col">
                                <label class="control-label">Empresa</label>
                                <select class="custom-select" id="empresa_list" name ="empresa" hidden="true" onchange="select_empresa()">
                                    ${list_filiados}
                                </select>
                                <input name ="empresa" id="empresa" type="text" onblur="Url_change()" class="form-control underlined"  placeholder="Nome da Empresa que sera visitada" required> 
                            </div>
                            <div class="col">
                                <label class="control-label">Endere�o</label>
                                <input type="text" id="endereco" name="endereco" class="form-control underlined" value="${endereco}" required>    
                            </div>
                        </div>

                        <input type="hidden" name="funcionario" id="funcionario" value="0">
                        <input type="hidden" name="hora" id="hora" value="0">
                        <input type="hidden" name="data" id="hora" value="${data}">
                        <input type="hidden" name="c1" id="c1" value="0">
                        <input type="hidden" name="Origem" id="origem" value="0">
                        <input type="hidden" name="c2" id="c2" value="0">
                        <input type="hidden" name="c3" id="c3" value="0">
                        <input type="hidden" name="c4" id="c4" value="0">
                        <input type="hidden" name="c5" id="c5" value="0">
                        <input type="hidden" name="c6" id="c6" value="0">
                        <input type="hidden" name="c7" id="c7" value="0">
                        <input type="hidden" name="visita" id="cvisita" value="0">
                        <input type="hidden" name="agendamento" id="c100" value="0">
                        <input type="hidden" name="visita_aleatoria" id="c101" value="0">
                        <div class="form-row">
                            <!--                            <div class="col"> 
                                                            <label class="control-label">Data</label>
                                                            <input name ="data" type="date" class="form-control underlined"  placeholder="(DD/MM/YYYY)" required>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container">
                        <h6 class="border-bottom border-gray pb-2 mb-4">Contato</h6>
                        <div class="form-row">
                            <div class="col">
                                <label class="control-label">Nome</label>
                                <input type="text" id="Nome" name="nome"  value="" class="form-control underlined"> 
                            </div>
                            <div class="col">
                                <label class="control-label">Fone Fixo</label>
                                <input type="text" id="fone" name="fone" value="${fonefixo}" class="form-control underlined"> 
                            </div>
                            <div class="col">
                                <label class="control-label">Celular</label>
                                <input type="text" id="celular" name="celular" value="${celular}" class="form-control underlined"> 
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label class="control-label">Email</label>
                                <input type="email" id="email" name="email" value="${email}" class="form-control underlined"> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-3 p-3 bg-white rounded box-shadow">
                    <h5 class="border-bottom border-gray pb-2 mb-4">Temas abordados:</h5> 
                    <div class="container">
                        <div class="form-row">
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="1" onchange="select_check(1)">
                                <label class="custom-control-label" for="1">NF-E</label>
                            </div>
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="2" onchange="select_check(2)">
                                <label class="custom-control-label" for="2">CDL Celular</label>
                            </div>
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="3" onchange="select_check(3)">
                                <label class="custom-control-label" for="3">Central De Cobran�a</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="4" onchange="select_check(4)">
                                <label class="custom-control-label" for="4">SPC</label>
                            </div>
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="5" onchange="select_check(5)">
                                <label class="custom-control-label" for="5">CDL Sa�de</label>
                            </div>
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="6" onchange="select_check(6)">
                                <label class="custom-control-label" for="6">Escola De Negocios</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="7" onchange="select_check(7)">
                                <label class="custom-control-label" for="7">Certificado Digital</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="checkbox" class="custom-control-input" id="80" onchange="Outros()">
                                <label class="custom-control-label" for="80">Outros:</label>
                                <input id="outros" name="outros" type="text" style="width: 70%" readonly="false">
                            </div>

                        </div>
                        <hr>
                        <div class="form-row text-center">
                            <label class="control-label">OBS</label>
                            <textarea class="form-control" rows="7" id="obs" name="obs"></textarea>
                        </div>

                        <!--                        <div class="row text-center">
                                                    <div class="col-sm">
                                                        <input type="checkbox" class="custom-control-input" id="visita" onchange="Visita(); select_check('visita')">
                                                        <label class="custom-control-label" for="visita">Agendar Visita</label>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
                <div class="text-center">
                    <input type="checkbox" class="custom-control-input" id="100" onchange="select_check(100)">
                    <label class="custom-control-label" for="100">Agendar Visita</label>
                </div>
                <div class="text-center">
                    <input type="checkbox" class="custom-control-input" id="101" onchange="select_check(101)">
                    <label class="custom-control-label" for="101">Agendar Visita Aleatoria</label>
                </div>
                <div id = "visita_body" style="display: none;">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Agenda</h6>
                        <div class="month">      
                            <ul>
                                <!--                            <li class="prev">&#10094;</li>
                                                            <li class="next">&#10095;</li>-->
                                <li id="mes">
                                    August<br>
                                    <span style="font-size:18px" id="ano">2017</span>
                                </li>
                            </ul>
                        </div>

                        <ul class="weekdays">
                            <li>Dom</li>
                            <li>Seg</li>
                            <li>Ter</li>
                            <li>Qua</li>
                            <li>Qui</li>
                            <li>Sex</li>
                            <li>Sab</li>

                        </ul>

                        <ul class="days" id="days">  
                            <a href="#"> <li> 1</li></a>
                        </ul>
                        ${table}
                    </div>
                </div>

                <div class="my-3 p-3 bg-white rounded box-shadow"> 
                    <div class="container  text-center">
                        <button type="submit" type="button" class="btn btn-success center">Salvar</button>
                        <a href="/WSCDL/Start" ><button  type="button" class="btn btn-danger center">Cancelar</button></a>
                    </div>
                </div>

            </form>
        </main>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
        <script src="../../assets/js/vendor/popper.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <script src="offcanvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script>
            function submitForm() {
                return confirm('Deseja salvar este contato?');
            }
                        function clicktipoRelacionamento(){
                            document.getElementById("empresa").setAttribute("hidden", true);
                            document.getElementById("empresa_list").removeAttribute("hidden");
                        }
                        function clicktipopros(){
                            document.getElementById("empresa_list").setAttribute("hidden", true);
                            document.getElementById("empresa").removeAttribute("hidden");
                        }
                        function select_empresa(){
                             document.getElementById("empresa").value = document.getElementById("empresa_list").value;
                             Url_change()
                        }
                        function Outros() {
                            if (document.getElementById("80").checked == true) {
                                document.getElementById("outros").removeAttribute("readonly");
                            } else {
                                document.getElementById("outros").setAttribute("readonly", true);
                                document.getElementById("outros").value = "";
                            }
                        }
                        function Url_change() {
                            data = "${data}";
                            Url_change(data);
                        }
                        function Url_change(data) {
                            if (data == null) {
                                data = "${data}";
                            }
                            document.location.href = "contato?data=" + data +
                                    "&empresa=" + document.getElementById("empresa").value +
                                    "&endereco=" + document.getElementById("endereco").value +
                                    "&Nome=" + document.getElementById("Nome").value +
                                    "&fone=" + document.getElementById("fone").value +
                                    "&email=" + document.getElementById("email").value +
                                    "&celular=" + document.getElementById("celular").value +
                                    "&relacionamento=" + document.getElementById("radio1").checked +
                                    "&Origem=" + document.getElementById("radio12").checked +
                                    "&prospeccao=" + document.getElementById("radio2").checked;
                        }
                        function select_check(id) {
                            if (document.getElementById(id).checked == true) {
                                document.getElementById("c" + id).value = "true";
                            } else {
                                document.getElementById("c" + id).value = "false";
                            }
                        }
                        function origem_select(id) {
                            document.getElementById("Origem").value = id;
                        }
                        function Visita() {
                            if (document.getElementById("visita").checked == false) {
                                document.getElementById("visita_body").style.display = "none";
                            } else {
                                document.getElementById("visita_body").style.display = "block";
                            }
                        }

                        $(function () {
                            $("#fone").mask("(00) 0000-0000");
                            $("#celular").mask("(00) 0 0000-0000");
                            displayCalendar();
                            var myDate = new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
                            document.getElementById("time").value = myDate;
                            var url_string = document.URL; //window.location.href
                            var url = new URL(url_string);
                            var xi = url.searchParams.get("relacionamento");
                            var xii = url.searchParams.get("Origem");
                            if ("${cdlcobranca}" == "true") {
                                document.getElementById("3").checked = true;
                            }
                            if ("${cdlsaude}" == "true") {
                                document.getElementById("5").checked = true;
                            }
                            if (xi == "true") {
                                clicktipoRelacionamento();
                                document.getElementById("radio1").checked = true;
                            } else {
                                clicktipopros();
                                document.getElementById("radio2").checked = true;
                            }

                            if (xii == "true") {
                                document.getElementById("radio12").checked = true;
                            } else {
                                document.getElementById("radio22").checked = true;
                            }
                            if (url.searchParams.get("empresa") != "") {
                                document.getElementById("empresa").value = url.searchParams.get("empresa");
                                if ("${empresa}" != "") {
                                    document.getElementById("empresa").value = "${empresa}";
                                }
                            }
                            if (url.searchParams.get("Nome") != "") {
                                document.getElementById("Nome").value = url.searchParams.get("Nome");
                                if ("${gerente}" != "") {
                                    document.getElementById("Nome").value = "${gerente}";
                                }
                            } else {
                                document.getElementById("Nome").value = "${empresa}";
                            }
                            if ("${endereco}" == "") {
                                document.getElementById("endereco").value = url.searchParams.get("endereco");
                            }
                            if ("${fonefixo}" == "") {
                                document.getElementById("fone").value = url.searchParams.get("fone");
                            }
                            if ("${celular}" == "") {
                                document.getElementById("celular").value = url.searchParams.get("celular");
                            }
                            if ("${email}" == "") {
                                document.getElementById("email").value = url.searchParams.get("email");
                            }
                        });
                        window.addEventListener('load', function () {

                        }, false);
                        var old_cell = 0;
                        function fun(id, hora, cell) {
                            var cell2 = old_cell;
                            document.getElementById("funcionario").value = id;
                            document.getElementById("hora").value = hora;
                            document.getElementById(cell).style.backgroundColor = "green";
                            old_cell = cell;
                            document.getElementById(cell2).style.backgroundColor = "";
                        }

                        function displayCalendar() {


                            var htmlContent = "";
                            var FebNumberOfDays = "";
                            var counter = 1;
                            var dateNow = new Date();
                            var month = dateNow.getMonth();
                            var nextMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
                            var prevMonth = month - 1;
                            var day = dateNow.getDate();
                            var year = dateNow.getFullYear();
                            //Determing if February (28,or 29)  
                            if (month == 1) {
                                if ((year % 100 != 0) && (year % 4 == 0) || (year % 400 == 0)) {
                                    FebNumberOfDays = 29;
                                } else {
                                    FebNumberOfDays = 28;
                                }
                            }

                            // names of months and week days.
                            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                            var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];
                            var dayPerMonth = ["31", "" + FebNumberOfDays + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"]

                            // days in previous month and next one , and day of week.
                            var nextDate = new Date(nextMonth + ' 1 ,' + year);
                            var weekdays = nextDate.getDay();
                            var weekdays2 = weekdays;
                            var numOfDays = dayPerMonth[month];
                            // this leave a white space for days of pervious month.
                            while (weekdays > 0) {
                                htmlContent += "<li></li>";
                                // used in next loop.
                                weekdays--;
                            }

                            // loop to build the calander body.
                            while (counter <= numOfDays) {

                                // When to start new line.
                                if (weekdays2 > 6) {
                                    weekdays2 = 0;
                                }

                                // if counter is current day.
                                // highlight current day using the CSS defined in header.
                                if (counter == ${day}) {
                                    htmlContent += "<a onclick=\"Url_change(" + year + "-" + (month + 1) + "-" + counter +
                                            ")\"><li><span class=\"active\">" + counter + "</span></li></a>";
                                } else {
                                    htmlContent += "<a onclick=\"Url_change(" + year + "-" + (month + 1) + "-" + counter +
                                            ")\"><li>" + counter + "</li></a>";
                                }

                                weekdays2++;
                                counter++;
                            }

                            document.getElementById("mes").innerHTML = monthNames[month] + "<br><span style=\"font-size:18px\" >" + year + "</span>";
                            document.getElementById("days").innerHTML = htmlContent;
                        }
        </script>
    </body>
</html>
