<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>CDL - Agenda</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="offcanvas.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            .flmenu{
                width: 160px!important;
                height: 100px!important;
                margin:5px;
                display:block !important;
                padding-top:23px;
                color:#f5f5f5;
            }
        </style>
    </head>


    <body class="bg-light">
        <!--        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">CDL - Agenda</a>
                    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#"> <span class="glyphicon glyphicon-home"></span> Dashboard <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Create">Novo Agendamento</a>
                            </li>
                    </div>
                </nav>-->

        <main role="main" class="container">
            <div style="background: #3f454f; height: 80px">
                <img src="Icones/Logomarca.png" style="position: relative;top: 15px;left: 70px; width: 70px" width="50px" height="50px"/>
                <div style="position: relative; left: 75%; top: -15px">
                </div>
            </div>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">DashBoard</li>
                    <div class="col">
                        <div class="float-right">
                            <a href="/WSCDL/login"><i class="material-icons" style="color: black">exit_to_app</i></a>
                        </div>
                        <div class="float-right" style="margin-right: 7px">
                            <a href="/WSCDL/Start"><i class="material-icons" style="color: black">home</i></a>
                        </div>
                    </div>
                </ol>

            </nav>
            <div class="my-3 p-3 bg-white rounded box-shadow">
                <div class="container"> 
                    <div class="form-row ">
                        <div class="col">
                            <label class="control-label">Funcionário</label>
                            <select class="custom-select my-1 mr-sm-2" id="Funcionario_search" name="funcionario">
                                ${select}
                            </select>
                        </div>
                        <div class="col">
                            <label class="control-label">Filiado</label>
                            <select class="custom-select my-1 mr-sm-2" id="Filiado_search" name="Filiado">
                                ${select_filiado}
                            </select>
                        </div>
                        <div class="col">
                            <label class="control-label">Data Inicial</label>
                            <input name ="data_inicial" id="data_inicial"  type="date" class="form-control underlined"  placeholder="DD/MM/YYYY" value="${data_inicial}" >
                        </div>
                        <div class="col">
                            <label class="control-label">Data Final</label>
                            <input name ="data_final" id="data_final"  type="date" class="form-control underlined"  placeholder="DD/MM/YYYY" value="${data_final}" >
                        </div>
                        <button type="button" class="btn btn-success" onclick="Url_change()">Pesquisar</button> 
                    </div>
                </div>
            </div>
            <!--      <div class="my-3 p-3 bg-white rounded box-shadow">
                    <h6 class="border-bottom border-gray pb-2 mb-0">Agenda</h6>
                    <div class="month">      
                      <ul>
                        <li class="prev">&#10094;</li>
                        <li class="next">&#10095;</li>
                        <li>
                          August<br>
                          <span style="font-size:18px">2017</span>
                        </li>
                      </ul>
                    </div>
            
                    <ul class="weekdays">
                      <li>Mo</li>
                      <li>Tu</li>
                      <li>We</li>
                      <li>Th</li>
                      <li>Fr</li>
                      <li>Sa</li>
                      <li>Su</li>
                    </ul>
            
                    <ul class="days">  
                      <li>1</li>
                      <li>2</li>
                      <li>3</li>
                      <li>4</li>
                      <li>5</li>
                      <li>6</li>
                      <li>7</li>
                      <li>8</li>
                      <li>9</li>
                      <li><span class="active">10</span></li>
                      <li>11</li>
                      <li>12</li>
                      <li>13</li>
                      <li>14</li>
                      <li>15</li>
                      <li>16</li>
                      <li>17</li>
                      <li>18</li>
                      <li>19</li>
                      <li>20</li>
                      <li>21</li>
                      <li>22</li>
                      <li>23</li>
                      <li>24</li>
                      <li>25</li>
                      <li>26</li>
                      <li>27</li>
                      <li>28</li>
                      <li>29</li>
                      <li>30</li>
                      <li>31</li>
                    </ul>
                  </div>
            -->
            <div class="my-3 p-3 bg-white rounded box-shadow">
                <table class="table table-striped" >
                    <thead>
                        <tr class = "bg-CDL" style=" color: white">
                            <th scope="col">Funcionário</th>
                            <th scope="col">Empresa</th>
                            <th scope="col">Data</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                            <th scope="col"></th>                            
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ${Table} 
                    </tbody>
                </table>
            </div>
            <div id="myModal" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class=" bg-CDL text-white text-center" style="height: 50px">
                            <h5 class="modal-title text-center" style="margin: 10px" id="exampleModalLongTitle">Bem-Vindo ${User}!!!</h5>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <div class="row" align="center" style="margin: 2px">
                                    <div class="col-sm">
                                        <div class="center"><a href="" data-dismiss="modal"> <i class="material-icons" style="color: #3c5899; font-size:42px;">home</i><p style="color: #3c5899">Home</p></a></div> 
                                    </div>
                                    <div class="col-sm">
                                        <div class="center"><a href="Create"><i class="material-icons" style="color: #3c5899; font-size:42px;">save</i><p style="color: #3c5899">Novo Agendamento</p></a></div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="funcionario" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form  action="/WSCDL/Funcionario" method="POST">
                            <div class="modal-header">
                                <h5 class="modal-title">Funcionario que ira realizar a visita</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="id" id="id" value="0">
                                <select class="custom-select my-1 mr-sm-2" id="funcionario" name="funcionario">
                                    ${select}
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" type="button" class="btn btn-primary">Salvar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="offcanvas.js"></script>
        <script type="text/javascript">
                            function Url_change() {
                                document.location.href = "?Metodo=contato&Filiado=" + document.getElementById("Filiado_search").value +
                                        "&funcionario=" + document.getElementById("Funcionario_search").value+
                                        "&data_inicial=" + document.getElementById("data_inicial").value+
                                        "&data_final=" + document.getElementById("data_final").value;
                            }
                            function fun(empresa, endereco, nome, fone, email, celular) {
                                document.location.href = "Create?empresa=" + empresa
                                        + "&endereco=" + endereco
                                        + "&Nome=" + nome
                                        + "&fone=" + fone
                                        + "&email=" + email
                                        + "&celular=" + celular
                                        + "&relacionamento=false"
                                        + "&prospeccao=false";
                            }
        </script>
    </body>
</html>
