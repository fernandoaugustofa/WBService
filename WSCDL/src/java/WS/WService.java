/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import com.faalpha.cdl.HorarioDao;
import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.Delphi;
import com.faalpha.cdl.Feedback;
import com.faalpha.cdl.FeedbackDAO;
import com.faalpha.cdl.Location;
import com.faalpha.cdl.User;
import com.faalpha.cdl.UserDAO;
import com.faalpha.cdl.filiado;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Kathy
 */
@Path("WS")
public class WService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of WService
     */
    public WService() {
    }

    /**
     * Retrieves representation of an instance of WS.WService
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Calendar c = Calendar.getInstance();
        c.setTime(Calendar.getInstance().getTime());
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String dataFormatada = formato.format(c.getTime());
        String sql = "";
        List<Agendamento> Agenda = HorarioDao.listar(sql);
        return hue.toJson(Agenda);
    }
    /**
     *
     * @param Funcionario
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{Funcionario}")
    public String callendar (@PathParam("Funcionario") String Funcionario){
        List<User> user = UserDAO.listar(" where Upper(username) = Upper('"+Funcionario+"') limit 1 ");
        User usr = user.get(0);
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
         String sql ="";
        if (usr.getNivel() < 3){
            sql = " where Upper(login.username) = Upper('" +Funcionario+"') order by data";
        }else{
            sql = " order by data";
        }
        List<Agendamento> Agenda = HorarioDao.listar(sql);
        return hue.toJson(Agenda);
    }
    /**
     *
     * @param Usuario
     * @param Senha
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("IniciarSessao/{Usuario}/{Senha}")
    public String Login (@PathParam("Usuario") String Usuario,
        @PathParam("Senha") String Senha){
        User login = new User();
        login.setName(Usuario);
        login.setPass(Senha);
        boolean pass = UserDAO.Verificar(login, 0);
        if (pass)
            return "True";
        else
            return "False";

    }


    /**
     *
     * @param content
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void feedback(String content) {
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Feedback Resposta = hue.fromJson(content, Feedback.class);
        FeedbackDAO.inserir(Resposta);
    }
    /**
     *
     * @param content
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("IniciarSessao")
    public void Inserir(String content) {
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        System.out.println("Json: "+ content);
        User Resposta = hue.fromJson(content, User.class);
        UserDAO.inserir(Resposta);
    }
    /**
     *
     * @param id
     * @param content
     */
    @PUT
    @Path("Start/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void start(@PathParam("id") String id,String content) {
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Location Resposta = hue.fromJson(content, Location.class);
        List<Agendamento> list = HorarioDao.listar("Where agenda.id = " + id);
        Agendamento Hora = list.get(0);
        Hora.setStatus(4);
        Calendar cal = Calendar.getInstance();
        Time Cas2 = new Time(cal.getTimeInMillis());
        Hora.setHora_Inicio(Cas2);
        Hora.setStart_lat(Resposta.latitude);
        Hora.setStart_log(Resposta.longitude);
        HorarioDao.atualizar(Hora);
    }
    /**
     *
     * @param id
     * @param content
     */
    @PUT
    @Path("Stop/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void Stop(@PathParam("id") String id,String content) {
        Gson hue = new Gson();
        Location Resposta = hue.fromJson(content, Location.class);
        List<Agendamento> list = HorarioDao.listar("Where agenda.id = " + id);
        Agendamento Hora = list.get(0);
        Calendar cal = Calendar.getInstance();
        Time Cas2 = new Time(cal.getTimeInMillis());
        Hora.setHora_Final(Cas2);
        Hora.setStop_lat(Resposta.latitude);
        Hora.setStop_lon(Resposta.longitude);
        HorarioDao.atualizar(Hora);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Empresas")
    public String Empresas (){
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<filiado> Empresas = Delphi.listar_all();
        return hue.toJson(Empresas);
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("Inserir")
    public void Inserir_agenda(String content) {
        Gson hue = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Agendamento Resposta = hue.fromJson(content, Agendamento.class);
        Resposta.setStatus(1);
        Resposta.setFuncionario_ID(0);
        HorarioDao.inserir(Resposta);
    }


}
