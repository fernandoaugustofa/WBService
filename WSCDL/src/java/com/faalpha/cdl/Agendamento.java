/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

import java.sql.Date;
import java.sql.Time;

/**
 * Classe que representa o modelo da base de dados
 * @author Kathy
 */
public class Agendamento {
    private Integer ID;
    private Integer Funcionario_ID;
    private String Funcionario;
    private String Empresa;
    private Date Data;
    private String Hora;
    private Integer Status;
    private String Endereco;
    private String Fone_fixo;
    private String Celular;
    private String Email;
    private Time Hora_Inicio;
    private Time Hora_Final;
    private Double Start_lat;
    private Double Start_log;
    private Double Stop_lat;
    private Double Stop_lon;
    private String Representante;
    public boolean contato;

/**
 * 
 * @param ID
 * @param Funcionario
 * @param Empresa
 * @param Funcionario_ID
 * @param Endereco
 * @param Data
 * @param Hora
 * @param Status
 * @param Hora_Inicio
 * @param Hora_Final
 * @param Start_lat
 * @param Start_log
 * @param Stop_lat
 * @param Stop_lon
 * @param Fone_fixo
 * @param celular
 * @param email 
 */
    public Agendamento(Integer ID, String Funcionario, String Empresa,Integer Funcionario_ID,
            String Endereco, Date Data, String Hora, Integer Status, Time Hora_Inicio,Time Hora_Final,Double Start_lat,
            Double Start_log, Double Stop_lat, Double Stop_lon,String Fone_fixo, String celular, String email, String Nome){
       this.ID = ID;
       this.Funcionario = Funcionario;
       this.Empresa = Empresa;
       this.Data = Data;
       this.Hora = Hora;
       this.Status = Status;
       this.Funcionario_ID = Funcionario_ID;
       this.Endereco = Endereco;
       this.Fone_fixo = Fone_fixo;
       this.Celular = celular;
       this.Email = email;
       this.Hora_Inicio = Hora_Inicio;
       this.Hora_Final = Hora_Final;
       this.Start_lat = Start_lat;
       this.Start_log = Start_log;
       this.Stop_lat = Stop_lat;
       this.Stop_lon = Stop_lon;
       this.Representante = Nome;
       
    }

    /**
     *
     * @return Chave primaria da tabela agenda, ultilizada para deletar e alterar o dados
     */
    public Integer getID() {
        return ID;
    }

    /**
     *
     * @return Retorna o nome do Funcionario
     */
    public String getFuncionario() {
        return Funcionario;
    }
    
    public String getNome(){
        return Representante;
    }
    
    public void setNome(String nome){
        this.Representante = nome;
    }
    
    /**
     *
     * @return Retrorna o Nome da Empresa
     */
    public String getEmpresa() {
        return Empresa;
    }

    /**
     *
     * @return Retorna a data da visita
     */
    public Date getData() {
        return Data;
    }

    /**
     *
     * @return Retorna a Hora da visita
     */
    public String getHora() {
        return Hora;
    }

    /**
     *
     * @return Retorna o status 1 - Pendente, 2 - Concluida, 3 - Falha
     */
    public Integer getStatus() {
        return Status;
    }

    /**
     *
     * @return Retorna o Id do funcionario que ira realizar ou realizou a tarefa
     */
    public Integer getFuncionario_ID() {
        return Funcionario_ID;
    }
    
    /**
     *
     * @return
     */
    public String getEndereco() {
        return Endereco;
    }

    /**
     *
     * @return
     */
    public Time getHora_Final() {
        return Hora_Final;
    }

    /**
     *
     * @return
     */
    public Time getHora_Inicio() {
        return Hora_Inicio;
    }

    /**
     *
     * @return
     */
    public Double getStart_lat() {
        return Start_lat;
    }

    /**
     *
     * @return
     */
    public Double getStart_log() {
        return Start_log;
    }

    /**
     *
     * @return
     */
    public Double getStop_lat() {
        return Stop_lat;
    }

    /**
     *
     * @return
     */
    public Double getStop_lon() {
        return Stop_lon;
    }

    /**
     *
     * @return
     */
    public String getCelular() {
        return Celular;
    }

    /**
     *
     * @return
     */
    public String getFone_fixo() {
        return Fone_fixo;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param celular
     */
    public void setCelular(String celular) {
        this.Celular = celular;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.Email = email;
    }

    /**
     *
     * @param fone_fixo
     */
    public void setFone_fixo(String fone_fixo) {
        Fone_fixo = fone_fixo;
    }

    /**
     *
     * @param start_lat
     */
    public void setStart_lat(Double start_lat) {
        Start_lat = start_lat;
    }

    /**
     *
     * @param start_log
     */
    public void setStart_log(Double start_log) {
        Start_log = start_log;
    }

    /**
     *
     * @param stop_lat
     */
    public void setStop_lat(Double stop_lat) {
        Stop_lat = stop_lat;
    }

    /**
     * 
     * @param stop_lon
     */
    public void setStop_lon(Double stop_lon) {
        Stop_lon = stop_lon;
    }

    /**
     *
     * @param ID (Chave Primaria)
     */
    public void setFuncionario_ID(Integer ID) {
        this.Funcionario_ID = ID;
    }
    
    /**
     *
     * @param ID (Chave Primaria)
     */
    public void setID(Integer ID) {
        this.ID = ID;
    }

    /**
     *
     * @param data Data do Agendamento
     */
    public void setData(Date data) {
        Data = data;
    }


    /**
     *
     * @param hora Hora do Agendamento
     */
    public void setHora(String hora) {
        Hora = hora;
    }

    /**
     *
     * @param status Retorna o status 1 - Pendente, 2 - Concluida, 3 - Falha
     */
    public void setStatus(Integer status) {
        Status = status;
    }

    /**
     *
     * @param funcionario
     */
    public void setFuncionario(String funcionario) {
        Funcionario = funcionario;
    }

    /**
     *
     * @param empresa
     */
    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    /**
     *
     * @param endereco
     */
    public void setEndereco(String endereco) {
        Endereco = endereco;
    }

    /**
     *
     * @param hora_Final
     */
    public void setHora_Final(Time hora_Final) {
        Hora_Final = hora_Final;
    }

    /**
     *
     * @param hora_Inicio
     */
    public void setHora_Inicio(Time hora_Inicio) {
        Hora_Inicio = hora_Inicio;
    }
}
