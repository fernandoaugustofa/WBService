/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Augusto
 */
public class UserDAO {

    public static List<User> listar() {
        return listar("");
    }

    public static List<User> listar(String query) {
        List<User> Funcionarios = new ArrayList<>();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select * from login " + query );
        try {
            ResultSet resultado = return_sql.executeQuery();
            while (resultado.next()) {
                User user = new User();
                user.setId(resultado.getInt("id"));
                user.setReal_Name(resultado.getString("nome"));
                user.setPass(resultado.getString("pass"));
                user.setNivel(resultado.getInt("nivel"));
                Funcionarios.add(user);
            }
        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return Funcionarios;
    }

    public static boolean atualizar() {
        String sql = "UPDATE login set "
                + "nome='Contato' where id=0";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {
            if (pst.executeUpdate() > 0) {
                retorno = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(HorarioDao.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }

        return retorno;

    }

    public static boolean Verificar(User user, int nivel) {
        PreparedStatement return_sql = Conexao.getPreparedStatement("select * from login where Upper(username) = Upper('"
                + user.getName() + "') and pass = '" + user.getPass() + "'");
        try {
            ResultSet resultado = return_sql.executeQuery();
            if (resultado.next()) {
                if (resultado.getInt("nivel") >= nivel) {
                    return true;
                }
            }
            return false;

        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return false;
    }

    public static boolean inserir(User user) {
        String sql = "INSERT INTO public.login(username, pass, id, nome) VALUES (?, ?, ?, ?);";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {
            pst.setString(1, user.getName());
            pst.setString(2, user.getPass());
            pst.setInt(3, user.getId());
            pst.setString(4, user.getReal_Name());
            if (pst.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }

        return retorno;

    }
}
