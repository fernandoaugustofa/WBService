/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

/**
 *
 * @author Kathy
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HorarioDao {

    public static List<Agendamento> listar() {
        List<Agendamento> Agenda = new ArrayList<>();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select agenda.*, login.username, login.nome as funcionario from agenda inner join login on funcionario_id = login.id order by data desc,hora");
        try {
            ResultSet resultado = return_sql.executeQuery();
            while (resultado.next()) {
                Agendamento Horario = new Agendamento(
                        resultado.getInt("id"),
                        resultado.getString("funcionario"),
                        resultado.getString("empresa"),
                        resultado.getInt("funcionario_id"),
                        resultado.getString("endereco"),
                        resultado.getDate("data"),
                        resultado.getString("hora"),
                        resultado.getInt("status"),
                        resultado.getTime("hora_inicio"),
                        resultado.getTime("hora_fim"),
                        resultado.getDouble("start_lat"),
                        resultado.getDouble("start_log"),
                        resultado.getDouble("stop_lat"),
                        resultado.getDouble("stop_log"),
                        resultado.getString("fone_fixo"),
                        resultado.getString("celular"),
                        resultado.getString("email"),
                        resultado.getString("nome"));
                Agenda.add(Horario);
            }
        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return Agenda;
    }

    public static List<Agendamento> listar(String Where) {
        List<Agendamento> Agenda = new ArrayList<>();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select agenda.*, login.username, login.nome as funcionario from agenda inner join login on funcionario_id = login.id " + Where);
        try {
            ResultSet resultado = return_sql.executeQuery();
            while (resultado.next()) {
                Agendamento Horario = new Agendamento(
                        resultado.getInt("id"),
                        resultado.getString("funcionario"),
                        resultado.getString("empresa"),
                        resultado.getInt("funcionario_id"),
                        resultado.getString("endereco"),
                        resultado.getDate("data"),
                        resultado.getString("hora"),
                        resultado.getInt("status"),
                        resultado.getTime("hora_inicio"),
                        resultado.getTime("hora_fim"),
                        resultado.getDouble("start_lat"),
                        resultado.getDouble("start_log"),
                        resultado.getDouble("stop_lat"),
                        resultado.getDouble("stop_log"),
                        resultado.getString("fone_fixo"),
                        resultado.getString("celular"),
                        resultado.getString("email"),
                        resultado.getString("nome"));
                Agenda.add(Horario);
            }
        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return Agenda;
    }

    public static Integer listar(String Where, Boolean Count) {
        List<Agendamento> Agenda = new ArrayList<>();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select count(*) as r from agenda " + Where);
        try {
            ResultSet resultado = return_sql.executeQuery();
            resultado.next();
            return resultado.getInt(1);

        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return -1;
    }

    public static boolean atualizar(Agendamento Horario) {
        String sql = "UPDATE agenda set "
                + "empresa=?,data=?,hora=?,Status=?, hora_inicio=?, hora_fim=?, "
                + "  start_lat =?,"
                + "  start_log =?,"
                + "  stop_lat =?,"
                + "  stop_log =?,"
                + "  funcionario_id =?,"
                + "  fone_fixo=?,"
                + "  celular =?,"
                + "  email =?, nome =?"
                + "where id=?";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {

            pst.setString(1, Horario.getEmpresa());
            pst.setDate(2, Horario.getData());
            pst.setString(3, Horario.getHora());
            pst.setInt(4, Horario.getStatus());
            pst.setTime(5, Horario.getHora_Inicio());
            pst.setTime(6, Horario.getHora_Final());
            pst.setDouble(7, Horario.getStart_lat());
            pst.setDouble(8, Horario.getStart_log());
            pst.setDouble(9, Horario.getStop_lat());
            pst.setDouble(10, Horario.getStop_lon());
            pst.setInt(11, Horario.getFuncionario_ID());
            pst.setString(12, Horario.getFone_fixo());
            pst.setString(13, Horario.getCelular());
            pst.setString(14, Horario.getEmail());
            pst.setString(15, Horario.getNome());
            pst.setInt(16, Horario.getID());
            if (pst.executeUpdate() > 0) {
                retorno = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(HorarioDao.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }

        return retorno;

    }

    public static boolean delete(Agendamento Horario) {
        String sql = "delete from feedbacks where id_agenda = ?;"
                + "delete from agenda where id=?;";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {
            pst.setInt(1, Horario.getID());
            pst.setInt(2, Horario.getID());
            if (pst.executeUpdate() > 0) {
                retorno = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(HorarioDao.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }

        return retorno;

    }

    public static boolean inserir(Agendamento Hora) {
        String sql = "INSERT INTO public.agenda"
                + "(empresa, data, hora, status, funcionario_id, endereco, fone_fixo, celular, email,nome,contato)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {
            Integer i = null;
            if (Hora.getFuncionario_ID() == null) {
                pst.setInt(5, 8);
            } else {
                pst.setInt(5, Hora.getFuncionario_ID());
            }
            pst.setString(1, Hora.getEmpresa());
            pst.setDate(2, Hora.getData());
            pst.setString(3, Hora.getHora());
            pst.setInt(4, 1);

            pst.setString(6, Hora.getEndereco());
            pst.setString(7, Hora.getFone_fixo());
            pst.setString(8, Hora.getCelular());
            pst.setString(9, Hora.getEmail());
            pst.setString(10, Hora.getNome());
            pst.setBoolean(11, Hora.contato);
            if (pst.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FeedbackDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }

        return retorno;

    }
}
