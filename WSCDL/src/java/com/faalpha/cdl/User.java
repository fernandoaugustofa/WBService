/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

/**
 *
 * @author Fernando Augusto
 */
public class User {
    private Integer id;
    private String Real_Name;
    private String Name;
    private String Pass;
    private Integer nivel;

    /**
     *
     * @return
     */
    public String getName() {
        return Name;
    }
    
    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getPass() {
        return Pass;
    }
    
    public Integer getNivel() {
        return nivel;
    }
    
    /**
     *
     * @return
     */
    public String getReal_Name() {
        return Real_Name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        Name = name;
    }
    
    /**
     *
     * @param name
     */
    public void setReal_Name(String name) {
        Real_Name = name;
    }

    /**
     *
     * @param pass
     */
    public void setPass(String pass) {
        Pass = pass;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    public void setNivel(Integer id) {
        this.nivel = id;
    }
}
