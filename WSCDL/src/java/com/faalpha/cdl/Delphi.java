/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathy
 */
public class Delphi {

    private static final String banco = "jdbc:postgresql://192.168.1.6:5432/solutions_anapolis_2019";
    private static final String driver = "org.postgresql.Driver";
    private static final String usuario = "desenv";
    private static final String senha = "#sol_fin";
    private static Connection con = null;

    public static Connection getConexao() {
        if (con == null) {
            try {
                Class.forName(driver);
                con = DriverManager.getConnection(banco, usuario, senha);
            } catch (ClassNotFoundException ex) {
                System.out.println("Não encontrou o driver");
            } catch (SQLException ex) {
                System.out.println("Erro ao conectar: "
                        + ex.getMessage());
            }
        }
        return con;
    }

    public static PreparedStatement getPreparedStatement(String sql) {
        if (con == null) {
            con = getConexao();
        }
        try {
            return con.prepareStatement(sql);
        } catch (SQLException e) {
            System.out.println("Erro de sql: "
                    + e.getMessage());
        }
        return null;
    }

    public static filiado listar(String Where) {
        List<Agendamento> Agenda = new ArrayList<>();
        PreparedStatement return_sql = getPreparedStatement("select *, (logradouro || ', Nº ' || numero || ', ' || case when complemento is not null then complemento end || ', ' || bairro || ' - ' || cidade) as endereco from filiado where Upper(nomefantasia) like upper('" + Where + "%')");
        try {
            ResultSet resultado = return_sql.executeQuery();
            resultado.next();
            filiado Filiado = new filiado();
            Filiado.full_name = resultado.getString("nomefantasia");
            Filiado.email = resultado.getString("email");
            Filiado.nome = resultado.getString("gerente");
            Filiado.endereco = resultado.getString("endereco");
            Filiado.celular = resultado.getString("telefone2");
            Filiado.fonefixo = resultado.getString("telefone1");
            return Filiado;
        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return null;
    }

    public static List<filiado> listar_all() {
        List<filiado> filiados = new ArrayList<>();
        PreparedStatement return_sql = getPreparedStatement("select *, (logradouro || ', Nº ' || numero || ', ' || complemento || ', ' || bairro || ' - ' || cidade) as endereco from filiado where situacao = 'A' order by nomefantasia");
        try {
            ResultSet resultado = return_sql.executeQuery();
            while (resultado.next()) {
                filiado Filiado = new filiado();
                Filiado.full_name = resultado.getString("nomefantasia");
                Filiado.email = resultado.getString("email");
                Filiado.nome = resultado.getString("gerente");
                Filiado.endereco = resultado.getString("endereco");
                Filiado.celular = resultado.getString("telefone2");
                Filiado.fonefixo = resultado.getString("telefone1");
                filiados.add(Filiado);
            }
            return filiados;

        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: "
                    + e.getMessage());
        }
        return null;
    }

    public static Feedback listar_servico(String Where) {
        PreparedStatement return_sql = getPreparedStatement("select * from filiado where Upper(nomefantasia) like upper('" + Where + "%')");
        try {
            ResultSet resultado = return_sql.executeQuery();
            resultado.next();
            Feedback Feed = new Feedback();
            Feed.check2 = resultado.getBoolean("cdl_celular");
            Feed.check3 = resultado.getBoolean("cdlcobranca");
            Feed.check5 = resultado.getBoolean("cdlsaude");
            return Feed;
        } catch (SQLException e) {
            System.out.println("Erro ao pesquisar: " + e.getMessage());
        }
        return null;
    }
}
