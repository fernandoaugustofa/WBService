/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.Delphi;
import com.faalpha.cdl.Feedback;
import com.faalpha.cdl.FeedbackDAO;
import com.faalpha.cdl.HorarioDao;
import com.faalpha.cdl.User;
import com.faalpha.cdl.UserDAO;
import com.faalpha.cdl.filiado;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andromeda
 */
@WebServlet(name = "contato", urlPatterns = {"/contato"})
public class contato extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public String removeAcentos(String str) {

        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String usuario = (String) req.getSession().getAttribute("User");
        if (usuario != null) {
            List<filiado> filiados = new ArrayList<>();
            filiados = Delphi.listar_all();
            String HTML = "";
            for (filiado f : filiados) {
                HTML += " <option value= \"" + f.full_name + "\">" + f.full_name + "</option>";
            }
            String query2 = req.getParameter("data");
            String relacionamento = req.getParameter("relacionamento");
            if (relacionamento != null) {
                if (relacionamento.equals("true")) {
                    String empresa_delphi = req.getParameter("empresa");
                    if (empresa_delphi != null) {
                        filiado Dados = Delphi.listar(empresa_delphi);
                        Feedback Dadoss = Delphi.listar_servico(empresa_delphi);
                        if (Dados != null) {
                            filiados = Delphi.listar_all();
                            HTML = "";
                            for (filiado f : filiados) {
                                String a = "";
                                if(f.full_name == null ? Dados.full_name == null : f.full_name.equals(Dados.full_name)){
                                    a = " selected ";
                                }
                                HTML += " <option "+a+" value= \"" + f.full_name + "\">" + f.full_name + "</option>";
                            }
                            System.out.println(Dados.full_name);
                            req.setAttribute("empresa", removeAcentos(Dados.full_name));
                            req.setAttribute("email", Dados.email);
                            req.setAttribute("gerente", Dados.nome);
                            System.out.println(Dados.endereco);
                            req.setAttribute("endereco", removeAcentos(Dados.endereco));
                            req.setAttribute("celular", Dados.celular);
                            req.setAttribute("fonefixo", Dados.fonefixo);
                            req.setAttribute("cdl_celular", Dadoss.check2);
                            req.setAttribute("cdlcobranca", Dadoss.check3);
                            req.setAttribute("cdlsaude", Dadoss.check5);
                        }
                    }
                }
            }
            req.setAttribute("list_filiados", HTML);

            String query = query2;
            System.out.println("query: " + query);
            List<User> Lista = UserDAO.listar("Where id <> 0");
            String select = "";
            for (User funcionario : Lista) {
                select += "<option value=\"" + funcionario.getId().toString() + "\">" + funcionario.getReal_Name() + "</option>";
            }
            Calendar c = Calendar.getInstance();
            c.setTime(Calendar.getInstance().getTime());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            System.out.println("dayOfWeek: " + dayOfWeek);
            String Calendario = "";
            Calendario += "<ul class=\"days\">";
            Calendario += "";
            String Table = "";
            String myTime = "08:00";
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            String newTime = df.format(cal.getTime());
            Table += " <div class=\"my-3 p-3 bg-white rounded box-shadow\">\n"
                    + "                <table class=\"table table-striped\" >\n"
                    + "                    <thead>\n"
                    + "                        <tr class = \"bg-CDL\" style=\" color: white\">\n"
                    + "                            <th scope=\"col\">Horario</th>\n";
            for (User funcionario : Lista) {
                Table
                        += "                            <th scope=\"col\">" + funcionario.getReal_Name() + "</th>\n";
            }
            Table
                    += "                        </tr>\n"
                    + "                    </thead>\n"
                    + "                    <tbody>\n";
            for (int i = 0; i <= 18; i++) {
                Table += "<tr>";
                newTime = df.format(cal.getTime());
                Table += "<td>" + newTime + "</td>";
                cal.add(Calendar.MINUTE, 30);
                for (User funcionario : Lista) {
                    String sql = "";
                    if (query == null) {
                        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat formato_day = new SimpleDateFormat("dd");
                        String dataFormatada = formato.format(c.getTime());
                        String dataFormatada2 = formato_day.format(c.getTime());
                        sql = " where funcionario_id = " + funcionario.getId().toString() + " and hora = '" + newTime + "' and data ='" + dataFormatada + "'";
                        req.setAttribute("data", dataFormatada);
                        req.setAttribute("day", dataFormatada2);
                    } else {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        java.sql.Date data = null;
                        try {
                            data = new java.sql.Date(format.parse(query).getTime());
                        } catch (ParseException ex) {
                            Logger.getLogger(contato.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        SimpleDateFormat formato_day = new SimpleDateFormat("dd");
                        sql = " where funcionario_id = " + funcionario.getId().toString() + " and hora = '" + newTime + "' and data = '" + query + "'";
                        req.setAttribute("data", query);
                        req.setAttribute("day", formato_day.format(data.getTime()));
                    }

                    if (HorarioDao.listar(sql, true) == 0) {
                        Table += "<td id =\"" + funcionario.getId().toString() + "" + i + "\" class=\"text-success\" onclick=\"fun(" + funcionario.getId().toString() + ",'" + newTime + "'," + funcionario.getId().toString() + "" + i + ")\">Livre</td>";
                    } else {
                        Table += "<td class=\"text-danger\">Ocupado</td>";
                    }
                }
                Table += "</tr>";
            }

            Table
                    += "                    </tbody>\n"
                    + "                </table>\n"
                    + "            </div><tr>";
            req.setAttribute("table", Table);
            req.setAttribute("select", select);
            RequestDispatcher rd = req.getRequestDispatcher("/contato.jsp");
            rd.forward(req, resp);
        } else {
            resp.sendRedirect("login");
        }
    }

}
