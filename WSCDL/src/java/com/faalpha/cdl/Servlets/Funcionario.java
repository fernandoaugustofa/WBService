/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.HorarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Augusto
 */
@WebServlet(name = "Funcionario", urlPatterns = {"/Funcionario"})
public class Funcionario extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String usuario = (String) request.getSession().getAttribute("User");
        if (usuario != null) {
            String funcionario = request.getParameter("funcionario");
            String id = request.getParameter("id");
            String hora = request.getParameter("hora");
            String data = request.getParameter("data");
            List<Agendamento> list = HorarioDao.listar("Where agenda.id = " + id);
            Agendamento Hora = list.get(0);
            Hora.setFuncionario_ID(Integer.parseInt(funcionario));
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            try {
                java.sql.Date Data = new java.sql.Date(fmt.parse(data).getTime());
                Hora.setData(Data);
                Hora.setHora(hora);
            } catch (ParseException ex) {
                Logger.getLogger(Funcionario.class.getName()).log(Level.SEVERE, null, ex);
            }
            HorarioDao.atualizar(Hora);
            response.sendRedirect(request.getContextPath());
        }
    }

}
