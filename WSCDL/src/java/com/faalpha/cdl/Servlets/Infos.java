/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.Feedback;
import com.faalpha.cdl.FeedbackDAO;
import com.faalpha.cdl.HorarioDao;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Augusto
 */
@WebServlet(name="Info", urlPatterns="/Info")
public class Infos extends HttpServlet {
 
     @Override
     protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
                 String usuario = (String) req.getSession().getAttribute("User");
        if (usuario != null) {
        String id = req.getParameter("id");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        List<Agendamento> list = HorarioDao.listar("Where agenda.id = " + id);
        Agendamento Hora = list.get(0);
        Feedback feed = FeedbackDAO.listar(id);
        String Status = "";
        if (Hora.getStatus() == 1){
            Status ="<small  class=\"text-info\">Pendente.</small >"; 
        }
        if (Hora.getStatus() == 2){
           Status ="<small class=\"text-success text-center\">Concluida.</small>"; 
        }
        if (Hora.getStatus() == 3){
           Status ="<small class=\"text-danger text-center\">Visita não realizada.</small>"; 
        }
        if (Hora.getStatus() == 4){
           Status ="<small class=\"text-warning text-center\">Em Andamento.</small>"; 
        }
        req.setAttribute("Empresa", Hora.getEmpresa());
        req.setAttribute("Funcionario", Hora.getFuncionario());
        req.setAttribute("Hora", Hora.getHora());
        req.setAttribute("Endereco", Hora.getEndereco());
        req.setAttribute("Data", formato.format(Hora.getData()));
        req.setAttribute("Hora_Inicio", Hora.getHora_Inicio());
        req.setAttribute("Hora_Final", Hora.getHora_Final());
        req.setAttribute("Start_lat", Hora.getStart_lat());
        req.setAttribute("Start_log", Hora.getStart_log());
        req.setAttribute("Stop_lat", Hora.getStop_lat());
        req.setAttribute("Stop_lon", Hora.getStop_lon());
        req.setAttribute("Status", Hora.getStatus());
        req.setAttribute("id_agenda",feed.ID);
        req.setAttribute("realizado",feed.Realizado);
        req.setAttribute("nfe",Bool(feed.check1));
        req.setAttribute("cdl_celular",Bool(feed.check2));
        req.setAttribute("central_cobranca",Bool(feed.check3));
        req.setAttribute("spc",Bool(feed.check4));
        req.setAttribute("cdl_saude",Bool(feed.check5));
        req.setAttribute("escola_de_negocios",Bool(feed.check6));
        req.setAttribute("certificado_digital",Bool(feed.check7));
        req.setAttribute("obs",feed.obs);
        req.setAttribute("h3Status",Status);
        RequestDispatcher rd = req.getRequestDispatcher("/info.jsp");
        rd.forward(req,resp);   
        }else{
              resp.sendRedirect("login");
        }
        
     }
     
     private String Bool(Boolean check){
         if(check){
             return "Checked";
         }
         return "";
     }
}
