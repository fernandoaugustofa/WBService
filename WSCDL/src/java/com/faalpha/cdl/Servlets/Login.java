/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.User;
import com.faalpha.cdl.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fernando Augusto
 */
@WebServlet(name="login", urlPatterns="/login")
public class Login extends HttpServlet {
 
     @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
       RequestDispatcher rd = req.getRequestDispatcher("/login/index.jsp");
       //User login_user = UserDAO.listar("").get(1);
       //req.setAttribute("user", login_user.getPass());
       rd.forward(req,resp);          

     }
     
     @Override
     protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String Usuario = request.getParameter("user");
        String Senha = request.getParameter("password");
        System.out.println("Usuario: "+ Usuario);
        System.out.println("Senha: "+ Senha);
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String hash2 = getSHA256(Senha);
        User login = new User();
        login.setName(Usuario);
        login.setPass(hash2);
        boolean pass = UserDAO.Verificar(login, 3);
       
        if (pass){
            User login_user = UserDAO.listar(" where Upper(username) = upper('"+Usuario+"')").get(0);
            HttpSession session = request.getSession(true); 
            session.setAttribute("User",login_user.getReal_Name());
            session.setAttribute("id",login_user.getId());
            response.sendRedirect("Start");                     
        }else{
            response.sendRedirect("login");
        }
    }         
     private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String getSHA256(String data) {
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(data.getBytes());
            byte[] byteData = md.digest();
            sb.append(bytesToHex(byteData));
        } catch(Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j];
            sb.append(hexArray[v >>> 4 & 0x0F]);
            sb.append(hexArray[v & 0x0F]);
        }
        return sb.toString();
    }
}
