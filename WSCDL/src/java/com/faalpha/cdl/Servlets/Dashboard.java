/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.Delphi;
import com.faalpha.cdl.Feedback;
import com.faalpha.cdl.FeedbackDAO;
import com.faalpha.cdl.HorarioDao;
import com.faalpha.cdl.User;
import com.faalpha.cdl.UserDAO;
import com.faalpha.cdl.filiado;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Augusto
 */
@WebServlet(name = "Dash", urlPatterns = "")
public class Dashboard extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String usuario = (String) req.getSession().getAttribute("User");
        if (usuario != null) {
            String contato = req.getParameter("Metodo");
            String Funcionario_search = req.getParameter("funcionario");
            String Empresa_search = req.getParameter("Filiado");
            String data_inicial = req.getParameter("data_inicial");
            String data_final = req.getParameter("data_final");
            String status = req.getParameter("status");
            req.setAttribute("data_inicial", data_inicial);
            req.setAttribute("data_final", data_final);
            req.setAttribute("Empresa_search", Empresa_search);
            req.setAttribute("Funcionario_search", Funcionario_search);
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

            String sql_contato = " and contato is false";
            String sql_contato2 = " where contato is false";
            String redirect = "/index.jsp";
            if (contato != null) {
                if (contato.equals("contato")) {
                    sql_contato = " and contato is true";
                    sql_contato2 = "where contato is true";
                    redirect = "/index_contato.jsp";
                }
            }
            if (Funcionario_search != null) {
                if (!Funcionario_search.equals("")) {
                    sql_contato += " and funcionario_id = " + Funcionario_search;
                    sql_contato2 += " and funcionario_id = " + Funcionario_search;
                }
            }
            if (status != null) {
                if (!status.equals("")) {
                    sql_contato += " and status = " + status;
                    sql_contato2 += " and status = " + status;
                }
            }
            if (data_inicial != null && data_final != null) {
                if (!data_inicial.equals("") && !data_final.equals("")) {
                    sql_contato += " and  data>= '" + data_inicial + "' and data <= '" + data_final + "'";
                    sql_contato2 += " and  data>= '" + data_inicial + "' and data <= '" + data_final + "'";
                }
            }
            if (Empresa_search != null) {
                if (!Empresa_search.equals("")) {
                    sql_contato += " and empresa = '" + Empresa_search + "'";
                    sql_contato2 += " and empresa = '" + Empresa_search + "'";
                }
            }
            sql_contato += " order by data desc";
            sql_contato2 += " order by data desc";
            Integer Max = HorarioDao.listar(sql_contato2, true);
            Integer Visitas_pendentes = HorarioDao.listar("where status = 1 " + sql_contato, true);
            Integer Visitas_concluidas = HorarioDao.listar("where status = 2 " + sql_contato, true);
            Integer Visitas_falha = HorarioDao.listar("where status = 3 " + sql_contato, true);
            Integer Visitas_andamento = HorarioDao.listar("where status = 4 " + sql_contato, true);
            String Status;
            String Funcionario;
            String Empresa;
            String Data;
            String Hora;
            List<Agendamento> Agenda = HorarioDao.listar(sql_contato2);
            List<Feedback> Feeds = FeedbackDAO.listar_limit("5");
            String HTML = "";
            String Feed = "";
            for (Feedback feeds : Feeds) {
                Feed += "<div class=\"media text-muted pt-3\">\n"
                        + "          <img data-src=\"holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1\" alt=\"\" class=\"mr-2 rounded\">\n"
                        + "          <p class=\"media-body pb-3 mb-0 small lh-125 border-bottom border-gray\">\n"
                        + "            <strong class=\"d-block text-gray-dark\">" + feeds.obs + "</strong>\n"
                        + feeds.Funcionario
                        + "          </p>\n"
                        + "        </div>";
            }
            for (Agendamento hora : Agenda) {
                Status = hora.getStatus().toString();
                Funcionario = hora.getFuncionario();
                Empresa = hora.getEmpresa();
                Data = formato.format(hora.getData());
                Hora = hora.getHora();
                HTML += "<tr>";
                HTML += "<td>" + Funcionario + "</td>";
                HTML += "<td>" + Empresa + "</td>";
                HTML += "<td>" + Data + "</td>";
                HTML += "<td>" + Hora + "</td>";
                Feedback feed = FeedbackDAO.listar(hora.getID().toString());
                HTML += "<td>" + feed.obs + "</td>";
                String s = null;
                if (Status.equals("1")) {
                    HTML += "<td class=\"text-info\">Pendente.</td>";
                    s = "text-info";
                }
                if (Status.equals("2")) {
                    HTML += "<td class=\"text-success\">Concluida.</td>";
                    s = "text-success";
                }
                if (Status.equals("3")) {
                    HTML += "<td class=\"text-danger\">Visita não realizada.</td>";
                    s = "text-danger";
                }
                if (Status.equals("4")) {
                    HTML += "<td class=\"text-warning\">Em Andamento.</td>";
                    s = "text-warning";
                }
                
                HTML += " <td><a href=\"Info?id=" + hora.getID().toString() + "\"><i class=\"material-icons " + s + "\">info</i></a></td>";
                if (contato == null) {
                    HTML += " <td><a onclick=\"fun(" + hora.getID().toString() + ",'" + Data + "','" + Hora + "'," + hora.getFuncionario_ID().toString() + ")\"><i class=\"material-icons text-sucess \">edit</i></a></td>";
                } else {
                    if (contato.equals("contato")) {//empresa, endereco, nome, fone, email, celular
                        HTML += " <td><a onclick=\"fun('" + Empresa + "','" + hora.getEndereco() + "','" + hora.getNome() + "','" + hora.getFone_fixo() + "','" + hora.getEmail() + "','" + hora.getCelular() + "')\"><i class=\"material-icons text-sucess \">control_point</i></a></td>";
                    }
                }
                HTML += " <td><a href=\"delete?id=" + hora.getID().toString() + "\"><i class=\"material-icons text-danger " + s + "\">delete</i></a></td>";
                HTML += "</tr>";
            }
            List<User> Lista = UserDAO.listar();
            String select = " <option value= \"\">Todos</option>";
            for (User funcionario : Lista) {
                select += "<option value=\"" + funcionario.getId().toString() + "\">" + funcionario.getReal_Name() + "</option>";
            }
            Lista = UserDAO.listar();
            String select2 = "";
            for (User funcionario : Lista) {
                select2 += "<option value=\"" + funcionario.getId().toString() + "\">" + funcionario.getReal_Name() + "</option>";
            }
            List<filiado> filiados;
            filiados = Delphi.listar_all();
            String F = " <option value= \"\">Todos</option>";
            for (filiado f : filiados) {
                F += " <option value= \"" + f.full_name + "\">" + f.full_name+"</option>";
            }
            req.setAttribute("select", select);
            req.setAttribute("select2", select2);
            req.setAttribute("select_filiado", F);
            req.setAttribute("Max", Max);
            req.setAttribute("User", usuario);
            req.setAttribute("Visitas_pendentes", Visitas_pendentes);
            req.setAttribute("Visitas_concluidas", Visitas_concluidas);
            req.setAttribute("Visitas_falha", Visitas_falha);
            req.setAttribute("Visitas_andamento", Visitas_andamento);
            req.setAttribute("Table", HTML);
            req.setAttribute("feed", Feed);
            RequestDispatcher rd = req.getRequestDispatcher(redirect);
            rd.forward(req, resp);
        } else {
            resp.sendRedirect("login");
        }
    }
}
