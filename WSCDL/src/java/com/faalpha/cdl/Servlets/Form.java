/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl.Servlets;

import com.faalpha.cdl.Agendamento;
import com.faalpha.cdl.Feedback;
import com.faalpha.cdl.Delphi;
import com.faalpha.cdl.FeedbackDAO;
import com.faalpha.cdl.HorarioDao;
import com.faalpha.cdl.User;
import com.faalpha.cdl.UserDAO;
import com.faalpha.cdl.filiado;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Augusto
 */
@WebServlet(name = "Form", urlPatterns = "/Create")
public class Form extends HttpServlet {

    public String removeAcentos(String str) {

        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String usuario = (String) req.getSession().getAttribute("User");
        if (usuario != null) {
            String query2 = req.getParameter("data");
            String id = req.getParameter("id");
            String relacionamento = req.getParameter("relacionamento");
            if (relacionamento != null) {
                if (relacionamento.equals("true")) {
                    String empresa_delphi = req.getParameter("empresa");
                    if (empresa_delphi != null) {
                        filiado Dados = Delphi.listar(empresa_delphi);
                        if (Dados != null) {
                            req.setAttribute("empresa", removeAcentos(Dados.full_name));
                            req.setAttribute("email", Dados.email);
                            req.setAttribute("endereco", removeAcentos(Dados.endereco));
                            req.setAttribute("celular", Dados.celular);
                            req.setAttribute("fonefixo", Dados.fonefixo);
                            req.setAttribute("nome", removeAcentos(Dados.nome));
                        }
                    }
                }
            }
            String query = query2;
            System.out.println("query: " + query);
            List<User> Lista = UserDAO.listar("Where id <> 0");
            String select = "";
            for (User funcionario : Lista) {
                select += "<option value=\"" + funcionario.getId().toString() + "\">" + funcionario.getReal_Name() + "</option>";
            }
            Calendar c = Calendar.getInstance();
            c.setTime(Calendar.getInstance().getTime());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            System.out.println("dayOfWeek: " + dayOfWeek);
            String Calendario = "";
            Calendario += "<ul class=\"days\">";
            Calendario += "";
            String Table = "";
            String myTime = "08:00";
            String myTime2;
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 8);
            cal.set(Calendar.MINUTE, 00);
            cal.set(Calendar.SECOND, 00);
            String newTime = df.format(cal.getTime());
            Table += " <div class=\"my-3 p-3 bg-white rounded box-shadow\">\n"
                    + "                <table class=\"table table-striped\" >\n"
                    + "                    <thead>\n"
                    + "                        <tr class = \"bg-CDL\" style=\" color: white\">\n"
                    + "                            <th scope=\"col\">Horario</th>\n";
            for (User funcionario : Lista) {
                Table
                        += "                            <th scope=\"col\">" + funcionario.getReal_Name() + "</th>\n";
            }
            Table
                    += "                        </tr>\n"
                    + "                    </thead>\n"
                    + "                    <tbody>\n";
            for (int i = 0; i <= 18; i++) {
                Table += "<tr>";
                newTime = df.format(cal.getTime());
                Table += "<td>" + newTime + "</td>";
                for (User funcionario : Lista) {
                    String sql = "";
                    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                    String dataFormatada = formato.format(c.getTime());
                    if (query == null) {

                        SimpleDateFormat formato_day = new SimpleDateFormat("dd");
                        String dataFormatada2 = formato_day.format(c.getTime());
                        sql = " where funcionario_id = " + funcionario.getId().toString() + " and hora = '" + newTime + "' and data ='" + dataFormatada + "'";
                        req.setAttribute("data", dataFormatada);
                        req.setAttribute("day", dataFormatada2);
                        query = dataFormatada;
                    } else {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        java.sql.Date data = null;
                        try {
                            data = new java.sql.Date(format.parse(query).getTime());
                        } catch (ParseException ex) {
                            Logger.getLogger(contato.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        SimpleDateFormat formato_day = new SimpleDateFormat("dd");
                        sql = " where funcionario_id = " + funcionario.getId().toString() + " and hora = '" + newTime + "' and data = '" + query + "'";
                        req.setAttribute("data", query);
                        req.setAttribute("day", formato_day.format(data.getTime()));
                    }

                    if (HorarioDao.listar(sql, true) == 0) {
                        if (query == null) {
                            if (cal2.after(cal)) {
                                Table += "<td class=\"text-danger\">Indisponivel</td>";
                            } else {
                                Table += "<td id =\"" + funcionario.getId().toString() + "" + i + "\" class=\"text-success\" onclick=\"fun(" + funcionario.getId().toString() + ",'" + newTime + "'," + funcionario.getId().toString() + "" + i + ")\">Livre</td>";
                            }
                        } else {
                            if (cal2.after(cal) && query.equals(dataFormatada)) {
                                Table += "<td class=\"text-danger\">Indisponivel</td>";
                            } else {
                                Table += "<td id =\"" + funcionario.getId().toString() + "" + i + "\" class=\"text-success\" onclick=\"fun(" + funcionario.getId().toString() + ",'" + newTime + "'," + funcionario.getId().toString() + "" + i + ")\">Livre</td>";
                            }
                        }
                    } else {
                        Table += "<td class=\"text-danger\">Ocupado</td>";
                    }
                }
                Table += "</tr>";
                cal.add(Calendar.MINUTE, 30);
            }

            Table
                    += "                    </tbody>\n"
                    + "                </table>\n"
                    + "            </div><tr>";
            req.setAttribute("table", Table);
            req.setAttribute("time", df.format(cal2.getTime()));
            req.setAttribute("select", select);
            RequestDispatcher rd = req.getRequestDispatcher("/cadastro.jsp");
            rd.forward(req, resp);
        } else {
            resp.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException, IOException {
        String usuario = (String) req.getSession().getAttribute("User");
        Integer Id_User = (Integer) req.getSession().getAttribute("id");

        if (usuario != null) {
            String empresa = req.getParameter("empresa");
            String data = req.getParameter("data");
            String hora = req.getParameter("hora");
            String data_visita = "";
            String outros = "";
            String hora_visita = "";
            String funcionario = req.getParameter("funcionario");
            String endereco = req.getParameter("endereco");
            String fone = req.getParameter("fone");
            String celular = req.getParameter("celular");
            String email = req.getParameter("email");
            String nome = req.getParameter("nome");
            String obs = "";
            String tipo = req.getParameter("tipo");
            Map params = req.getParameterMap();
            boolean c1 = false;
            boolean c2 = false;
            boolean c3 = false;
            boolean c4 = false;
            boolean c5 = false;
            boolean c6 = false;
            boolean c7 = false;
            boolean visita = false;
            boolean Visita_aleatoria = false;
            boolean agendamento = false;
            if (tipo.equals("Visita")) {
                outros = req.getParameter("outros");
                obs = req.getParameter("obs");
                data_visita = req.getParameter("data_visita");
                System.out.println("data_visita: " + data_visita);
                hora_visita = req.getParameter("hora_visita");
                c1 = "true".equals(req.getParameter("c1"));
                c2 = "true".equals(req.getParameter("c2"));
                c3 = "true".equals(req.getParameter("c3"));
                c4 = "true".equals(req.getParameter("c4"));
                c5 = "true".equals(req.getParameter("c5"));
                c6 = "true".equals(req.getParameter("c6"));
                c7 = "true".equals(req.getParameter("c7"));
                visita = "true".equals(req.getParameter("visita"));
                Visita_aleatoria = "true".equals(req.getParameter("visita_aleatoria"));
                agendamento = "true".equals(req.getParameter("agendamento"));
                System.out.println("Visita: " + req.getParameter("visita"));
            }
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date Data;
            java.sql.Date Data_visita;
            try {
                Agendamento Novo;
                if (tipo.equals("Agendamento") || visita) {
                    Data = new java.sql.Date(fmt.parse(data).getTime());

                    Novo = new Agendamento(null, null, empresa, Integer.parseInt(funcionario),
                            endereco, Data, hora, 1, null, null, null,
                            null, null, null, fone, celular, email, nome);
                    Novo.contato = false;
                    HorarioDao.inserir(Novo);
                }
                if (tipo.equals("Visita")) {
                    Data_visita = new java.sql.Date(fmt.parse(req.getParameter("data_visita")).getTime());
                    if (Visita_aleatoria) {
                        Novo = new Agendamento(null, null, empresa, Id_User,
                                endereco, Data_visita, "Aleatoria", 1, null, null, null,
                                null, null, null, fone, celular, email, nome);
                        Novo.contato = false;
                        HorarioDao.inserir(Novo);
                    }
                    Novo = new Agendamento(null, null, empresa, Id_User,
                            endereco, Data_visita, hora_visita, 1, null, null, null,
                            null, null, null, fone, celular, email, nome);
                    Novo.contato = true;
                    HorarioDao.inserir(Novo);
                    List<Agendamento> list = HorarioDao.listar(" order by id desc limit 1");
                    Agendamento Hora = list.get(0);
                    Feedback AA = new Feedback();
                    AA.ID = Hora.getID();
                    AA.Realizado = false;
                    AA.check1 = c1;
                    AA.check2 = c2;
                    AA.check3 = c3;
                    AA.check4 = c4;
                    AA.check5 = c5;
                    AA.check6 = c6;
                    AA.check7 = c7;
                    AA.obs = obs;
                    AA.outros = outros;
                    AA.Realizado = true;
                    AA.Contato = true;
                    FeedbackDAO.inserir(AA);
                }
                if (!agendamento) {
                    resp.sendRedirect(req.getContextPath() + "/Start");
                } else {
                    resp.sendRedirect(req.getContextPath() + "/Create?data=" + req.getParameter("data_visita")
                            + "&empresa=" + empresa
                            + "&endereco=" + endereco
                            + "&Nome=" + nome
                            + "&fone=" + fone
                            + "&email=" + email
                            + "&celular=" + celular
                            + "&relacionamento=" + false
                            + "&prospeccao=" + false);
                }
            } catch (ParseException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            resp.sendRedirect("login");
        }

    }

}
