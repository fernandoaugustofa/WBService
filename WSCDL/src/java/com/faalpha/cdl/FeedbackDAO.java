/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faalpha.cdl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Augusto
 */
public class FeedbackDAO {
   public static boolean inserir(Feedback feed)
    {
        String sql = "INSERT INTO feedbacks(id_agenda, realizado, nfe, cdl_celular, central_cobranca, spc, cdl_saude, escola_de_negocios"
                + ", certificado_digital, obs, outros) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Boolean retorno = false;
        PreparedStatement pst = Conexao.getPreparedStatement(sql);
        try {
            pst.setInt(1, feed.ID);
            pst.setBoolean(2, feed.Realizado);
            pst.setBoolean(3, feed.check1);
            pst.setBoolean(4, feed.check2);
            pst.setBoolean(5, feed.check3);
            pst.setBoolean(6, feed.check4);
            pst.setBoolean(7, feed.check5);
            pst.setBoolean(8, feed.check6);
            pst.setBoolean(9, feed.check7);
            pst.setString(10, feed.obs);
            pst.setString(11, feed.outros);
            if(pst.executeUpdate()>0)
            {
                List<Agendamento> list = HorarioDao.listar("Where agenda.id = " + feed.ID);
                Agendamento Hora = list.get(0);
                if (feed.Realizado){
                Hora.setStatus(2);
                }else{
                Hora.setStatus(3);  
                }
                HorarioDao.atualizar(Hora);
                retorno = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FeedbackDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        }
        
        return retorno;
    
} 
    public static Feedback listar(String id){
        Feedback feed = new Feedback();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select * from feedbacks where id_agenda = " + id);
        try{
            ResultSet resultado = return_sql.executeQuery();
            while(resultado.next()){
                feed.ID =resultado.getInt("id_agenda");
                feed.Realizado =resultado.getBoolean("realizado");
                feed.check1 =resultado.getBoolean("nfe");
                feed.check2 =resultado.getBoolean("cdl_celular");
                feed.check3 =resultado.getBoolean("central_cobranca");
                feed.check4 =resultado.getBoolean("spc");
                feed.check5 =resultado.getBoolean("cdl_saude");
                feed.check6 =resultado.getBoolean("escola_de_negocios");
                feed.check7 =resultado.getBoolean("certificado_digital");
                feed.obs =resultado.getString("obs");
            }
        }catch(SQLException e){
            System.out.println("Erro ao pesquisar: "+
              e.getMessage());   
        }
       return feed; 
    }
    
    public static List<Feedback> listar_limit(String limit){
       
         List<Feedback> feeds = new ArrayList<>();
        PreparedStatement return_sql = Conexao.getPreparedStatement("select feedbacks.*, username from feedbacks inner"
                + " join agenda on id_agenda = agenda.id inner join login on funcionario_id = login.id order by id desc limit  " + limit);
        try{
            ResultSet resultado = return_sql.executeQuery();
            while(resultado.next()){
                 Feedback feed = new Feedback();
                feed.ID =resultado.getInt("id_agenda");
                feed.Realizado =resultado.getBoolean("realizado");
                feed.check1 =resultado.getBoolean("nfe");
                feed.check2 =resultado.getBoolean("cdl_celular");
                feed.check3 =resultado.getBoolean("central_cobranca");
                feed.check4 =resultado.getBoolean("spc");
                feed.check5 =resultado.getBoolean("cdl_saude");
                feed.check6 =resultado.getBoolean("escola_de_negocios");
                feed.check7 =resultado.getBoolean("certificado_digital");
                feed.obs =resultado.getString("obs");
                feed.Funcionario= resultado.getString("username");
                feeds.add(feed);
            }
        }catch(SQLException e){
            System.out.println("Erro ao pesquisar: "+
              e.getMessage());   
        }
       return feeds; 
    }
}
